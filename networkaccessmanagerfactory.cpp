#include "networkaccessmanagerfactory.h"
#include "customnetworkaccessmanager.h"

#include <QNetworkDiskCache>
#include <QDesktopServices>

NetworkAccessManagerFactory::NetworkAccessManagerFactory(QString p_userAgent) : QDeclarativeNetworkAccessManagerFactory(), __userAgent(p_userAgent)
{
}

QNetworkAccessManager* NetworkAccessManagerFactory::create(QObject* parent)
{
    CustomNetworkAccessManager* manager = new CustomNetworkAccessManager(__userAgent, parent);

//    QNetworkDiskCache *diskCache = new QNetworkDiskCache(manager);
//    diskCache->setCacheDirectory(QDesktopServices::storageLocation(QDesktopServices::CacheLocation));
//    diskCache->setMaximumCacheSize(1024 * 1024);

//    manager->setCache(diskCache);

    return manager;
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="77"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="154"/>
        <source> of </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <source>Unsubscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="37"/>
        <source>Mark All as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="41"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="119"/>
        <source>Error in connection to Google</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="160"/>
        <source>Google Sign-In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="178"/>
        <location filename="../qml/gNewsReader/js/main.js" line="188"/>
        <source>Updating Subscriptions..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="335"/>
        <source>All News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="339"/>
        <source>All Unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="343"/>
        <source>All Starred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="352"/>
        <source>Loading Feed..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="406"/>
        <source> of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="447"/>
        <source>You have no matching Feed Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="593"/>
        <source>You have no subscribed feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="593"/>
        <source>No Feeds with Unread Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="656"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="659"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="663"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="88"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="130"/>
        <source>gNewsReader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="214"/>
        <source>Send Debug info to Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="218"/>
        <source>Subscribe to New Feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="222"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="222"/>
        <source>Show Unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="226"/>
        <source>Load Starred Feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="230"/>
        <source>Load New Feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="234"/>
        <source>Load All Feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="238"/>
        <source>Clear Authorization Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Mark All as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="300"/>
        <source>Load More Items..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="304"/>
        <source>Show All Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="304"/>
        <source>Show Unread Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="376"/>
        <source>Open in Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="380"/>
        <source>Copy URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="448"/>
        <source>Google</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

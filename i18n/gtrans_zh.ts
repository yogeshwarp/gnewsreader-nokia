<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>关于此程序</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation>版本号: %1</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>开源的Google Reader客户端，基于Symbian^3/MeeGo平台。&lt;br&gt;&lt;br&gt;如需Bug提交、问题反馈或新功能需求，&lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;请点击项目主页&lt;/a&gt; 或发邮件给作者 &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translation>隐私政策</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation>UI设计及美化</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version 1.50</source>
        <translation>版本号 1.50</translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation>版本号 1.60</translation>
    </message>
    <message>
        <source>Version 1.60</source>
        <translation>版本号 1.60</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>基于Symbian^3的Google Reader开源客户端。&lt;br&gt;&lt;br&gt;如需提交Bug、反馈意见以及功能需求 &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;请到此项目的网站&lt;/a&gt; 或者发e-mail给作者 &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation>开发团队</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation>作者（twitter: @yogeshwarp）</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>界面改进和美化&lt;br&gt;（twitter: @gxsaurav）</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation>翻译人员</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation>添加新的订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation>订阅源地址或要搜索的关键词</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation>源标题（可选）</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation>关于gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation>使用浅色主题</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation>只加载未读项</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation>图片自适应缩放</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation>载入完整的HTML内容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation>使用大字体</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation>自动下载图片</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation>使用手势操作</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation>清除账户数据</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Dark Article Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="225"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>导出订阅源（需重新登录）</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="9"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="10"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation>添加或修改标签</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation>使用逗号分隔标签</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation>在这里输入标签</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="78"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="155"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="172"/>
        <source> of </source>
        <translation> / </translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation>标记为已读</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation>保持为未读状态</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation>删除星标</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation>加注星标</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation>发送至</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation>订阅</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>暂不支持在新窗口中打开链接。如果要创建或找回您的谷歌账户，请在系统浏览器中完成该操作</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation>把订阅源添加到文件夹</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation>输入新文件夹名称</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>ago</source>
        <translation>前</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>自现在起</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>刚刚</translation>
    </message>
    <message>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>小时</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>小时</translation>
    </message>
    <message>
        <source>day</source>
        <translation>天</translation>
    </message>
    <message>
        <source>days</source>
        <translation>天</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>周</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>周</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>个月</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>个月</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>年</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>年</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%1%2%3</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation>下拉可以刷新</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation>上拉继续加载</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation>松开之后加载</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation>松开之后刷新</translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation>登录到</translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation>Pocket</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation>分享文章</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation>记住我</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation>隐私政策</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation>在这里输入文字</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation>重命名订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Unsubscribe</source>
        <translation>取消订阅</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <source>Mark All as Read</source>
        <translation>全部标为已读</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Confirm Action</source>
        <translation>确认操作</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="36"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="45"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation>移至文件夹</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation>订阅的条目</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>网络连接错误。错误代码：</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="116"/>
        <source>Error in connection to Google</source>
        <translation>无法连接到谷歌服务器</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="157"/>
        <source>Google Sign-In</source>
        <translation>登入谷歌账户</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="176"/>
        <location filename="../qml/gNewsReader/js/main.js" line="186"/>
        <source>Updating Subscriptions..</source>
        <translation>正在更新数据..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="333"/>
        <source>All News</source>
        <translation>所有的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="337"/>
        <source>All Unread</source>
        <translation>所有未读的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="341"/>
        <source>All Starred</source>
        <translation>所有加星标的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="350"/>
        <location filename="../qml/gNewsReader/js/main.js" line="358"/>
        <source>Loading Feed..</source>
        <translation>正在加载..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="414"/>
        <source> of </source>
        <translation> / </translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="454"/>
        <source>You have no matching Feed Items</source>
        <translation>未找到相符的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>You have no subscribed feeds</source>
        <translation>你没有任何订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>No Feeds with Unread Items</source>
        <translation>没有未读的条目了</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="689"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation>发送成功(%1)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>发送成功(params)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service</source>
        <translation>成功发送链接到服务器</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="692"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>无法登入服务器，请检查您的用户名或密码是否正确</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="696"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>发送过程中出现未知错误。请稍后重试</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="86"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="128"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Go Online</source>
        <translation>在线模式</translation>
    </message>
    <message>
        <source>Go Offline</source>
        <translation>离线模式</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="216"/>
        <source>Subscribe to New Feed</source>
        <translation>添加新的订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show All</source>
        <translation>显示全部订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show Unread</source>
        <translation>显示有未读项的订阅源</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="232"/>
        <source>Load Starred Feeds</source>
        <translation>载入加星标的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="224"/>
        <source>Load New Feeds</source>
        <translation>载入所有未读的条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="228"/>
        <source>Load All Feeds</source>
        <translation>载入所有条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="236"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <source>Clear Authorization Data</source>
        <translation>清除账户数据</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Confirm Action</source>
        <translation>确认操作</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="290"/>
        <source>Mark All as Read</source>
        <translation>全部标记为已读</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Mark all as Read</source>
        <translation>全部标记为已读</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Load More Items..</source>
        <translation>载入更多..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show All Items</source>
        <translation>显示全部条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show Unread Items</source>
        <translation>显示未读条目</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="371"/>
        <source>Open in Browser</source>
        <translation>在浏览器中打开</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="375"/>
        <source>Copy URL</source>
        <translation>复制网址</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>取消保持为未读状态</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>保持为未读状态</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>分享</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="443"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation>已清除登录信息：Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation>已清除登录信息：Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation>已清除登录信息：Facebook</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation>已清除登录信息：Pocket</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation>成功发布新推</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation>成功发送到Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation>成功在Facebook上分享</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>认证错误：请检查用户名和密码是否正确</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation>未知错误：请稍候重试</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fi_FI">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>Tietoja sovelluksesta</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation>Versio %1</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Avoimen lähdekoodin sovellus Google syötteenlukijalle Symbian^3/MeeGo ympäristöön.&lt;br&gt;&lt;br&gt;Vikailmoitukset, palaute ja toiveet &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Projektin sivusto&lt;/a&gt; tai tekijän sähköposti &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translation>Yksityisyydensuoja</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation>Tekijä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation>Käyttöliittymäpalaute &amp; Parannukset / Grafiikka&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation>puola</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation>kiina (yksinkert)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation>kiina (perint)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation>bulgaria</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation>kroatia</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation>italia</translation>
    </message>
    <message>
        <source>Version 1.50</source>
        <translation>Versio 1.50</translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation>Versio 1.60</translation>
    </message>
    <message>
        <source>Version 1.60</source>
        <translation>Versio 1.60</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Avoimen lähdekoodin Google syötteenlukija-sovellus Symbian^3 käyttöjärjestelmään.&lt;br&gt;&lt;br&gt;Vikailmoituksia, palautetta tai ehdotuksia antaaksesi &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;mene projektin kotisivulle&lt;/a&gt; tai lähetä sähköpostia tekijälle &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation>Projektiryhmä</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation>Tekijä (twitter: @yogeshwarp)</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>Käyttöliittymäpalaute ja parannukset / Grafiikka&lt;br&gt;(twitter: @gx_saurav)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation>Käännökset</translation>
    </message>
    <message>
        <source>Polish Language (twitter: @pagaw102)</source>
        <translation>puola (twitter: @pagaw102)</translation>
    </message>
    <message>
        <source>Simplified Chinese (twitter: @yeatse)</source>
        <translation>kiina, yksinkertaistettu (twitter: @yeatse)</translation>
    </message>
    <message>
        <source>Traditional Chinese (twitter: @garykb8)</source>
        <translation>kiina, perinteinen (twitter: @garykb8)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation>saksa</translation>
    </message>
    <message>
        <source>Bulgarian Language (twitter: @acnapyx)</source>
        <translation>bulgaria (twitter: @acnapyx)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation>suomi</translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation>Tilaa syöte Google syötteenlukijaan</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation>Syötteen URL-osoite tai hakutermi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation>Otsikko (valinnainen)</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation>Tietoja gNewsReaderista</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation>Vaalea teema</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation>Piilota kaikki luetut otsikot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation>Automaattinen kuvan sovitus</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation>Täysi HTML-sisältö</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation>Suurempi tekstikoko</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation>Hae kuvat automaattisesti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation>Käytä pyyhkäisyeleitä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation>Tyhjennä kirjautumistiedot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Dark Article Theme</source>
        <translation>Tumma artikkelien teema</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="225"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>Vie OPML-tiedosto (vaatii Google-kirjautumisen)</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="9"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="10"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation>Lisää tai muokkaa tunnisteita</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation>Erota tunnisteet pilkulla</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation>Kirjoita tunnisteet tähän</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation>Tallenna</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="78"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="155"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="172"/>
        <source> of </source>
        <translation> / </translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation>Merkitse luetuksi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation>Jätä lukemattomaksi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation>Poista tähti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation>Lisää tähti</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation>Lähetä</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation>Hakutulokset</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation>Tilaa</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>Sovellus ei tällä hetkellä tue linkkejä jotka avaavat uuden selainikkunan. Jos yrität palauttaa tai luoda Google-tilin, käytä laitteesi web-selainta</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation>Lisää syöte kansioon</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation>Luo uusi kansio</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>gNewsReader application does not use any personal data for analytics, in fact there is no analytics solution being used.

All the user authorization for various sharing services (twitter/facebook/instapaper/pocket) is collected by user concent only and optional.
All authorization information is only stored on the phone in application specific storage and not in shared area. All network call involving transfer of authorization information is made over secure HTTP channel. Additionally authorization information stored locally can be cleared anytime by the user.

To clear google id authorization use main application menu, any sharing services stored authorization can be cleared using settings page</source>
        <translation>gNewsReader-sovellus ei käytä mitään henkilökohtaisia tietoja analyysiin, se ei itse asiassa suorita analyysejä lainkaan.
        
Kaikki käyttäjätiedot jakamispalveluihin (twitter/facebook/instapaper/pocket) kerätään vain käyttäjän suostumuksella, eivätkä ne ole pakollisia.
Kirjautumistiedot talletetaan ainoastaan puhelimen sovelluskohtaiseen muistiin - ei jaetulle alueelle. Kaikki verkkoyhteydet joissa siirretään kirjautumistietoja tehdään suojatun HTTP kanavan kautta. Lisäksi käyttäjä voi koska tahansa poistaa puhelimeen tallennetut kirjautumistiedot.</translation>
    </message>
    <message>
        <source>ago</source>
        <translation>sitten</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>Aiemmin</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>juuri nyt</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>min</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>h</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>h</translation>
    </message>
    <message>
        <source>day</source>
        <translation>pv</translation>
    </message>
    <message>
        <source>days</source>
        <translation>pv</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>vko</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>vko</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>kk</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>kk</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>v</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>v</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%1 %2 %3</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation>Päivitä vetämällä alas</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation>Lataa lisää vetämällä ylös</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation>Lataa lisää päästämällä irti</translation>
    </message>
    <message>
        <source>Click to Refresh</source>
        <translation>Päivitä klikkaamalla</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation>Päivitä päästämällä irti</translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation>Kirjaudu palveluun </translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation>Pocket</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation>Jaa artikkeli ja URL</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation>Käyttäjätunnus</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation>Tallenna kirjautumistiedot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation>Yksityisyydensuoja</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation>Kirjoita tähän</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation>Nimeä syöte uudelleen</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Unsubscribe</source>
        <translation>Peruuta tilaus</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <source>Mark All as Read</source>
        <translation>Merkitse kaikki luetuiksi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Confirm Action</source>
        <translation>Vahvista toimenpide</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="36"/>
        <source>Rename</source>
        <translation>Nimeä uudelleen</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="45"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation>Näytä kansiossa</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation>Tilaukset</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>Yhteysvirhe. Virhekoodi:</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="116"/>
        <source>Error in connection to Google</source>
        <translation>Virhe Google-yhteydessä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="157"/>
        <source>Google Sign-In</source>
        <translation>Kirjautuminen Googleen</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="176"/>
        <location filename="../qml/gNewsReader/js/main.js" line="186"/>
        <source>Updating Subscriptions..</source>
        <translation>Päivitetään tilauksia..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="333"/>
        <source>All News</source>
        <translation>Kaikki uutiset</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="337"/>
        <source>All Unread</source>
        <translation>Kaikki lukemattomat</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="341"/>
        <source>All Starred</source>
        <translation>Kaikki tähdelliset</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="350"/>
        <location filename="../qml/gNewsReader/js/main.js" line="358"/>
        <source>Loading Feed..</source>
        <translation>Haetaan syötettä..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="414"/>
        <source> of </source>
        <translation> / </translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="454"/>
        <source>You have no matching Feed Items</source>
        <translation>Ei yhtään otsikkoa</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>You have no subscribed feeds</source>
        <translation>Ei yhtään tilattua syötettä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>No Feeds with Unread Items</source>
        <translation>Ei yhtään lukematonta otsikkoa</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="689"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation>Linkin lähetys palveluun (%1) onnistui</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>Linkin lähettäminen palveluun onnistui (parametrit)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service</source>
        <translation>Linkin lähettäminen palveluun onnistui</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="692"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>Palveluun kirjautuminen epäonnistui, käyttäjätunnus tai salasana saattavat olla virheellisiä</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="696"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>Tuntematon virhe lähetyksessä. Yritä hetken kuluttua uudelleen</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="86"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="128"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Go Online</source>
        <translation>Muodosta yhteys</translation>
    </message>
    <message>
        <source>Go Offline</source>
        <translation>Katkaise yhteys</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="216"/>
        <source>Subscribe to New Feed</source>
        <translation>Tilaa uusi syöte</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show All</source>
        <translation>Näytä kaikki</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show Unread</source>
        <translation>Näytä lukemattomat</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="232"/>
        <source>Load Starred Feeds</source>
        <translation>Hae tähdelliset syötteet</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="224"/>
        <source>Load New Feeds</source>
        <translation>Hae uudet syötteet</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="228"/>
        <source>Load All Feeds</source>
        <translation>Hae kaikki syötteet</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="236"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <source>Clear Authorization Data</source>
        <translation>Tyhjennä kirjautumistiedot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Confirm Action</source>
        <translation>Vahvista toimenpide</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="290"/>
        <source>Mark All as Read</source>
        <translation>Merkitse kaikki luetuiksi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Mark all as Read</source>
        <translation>Merkitse kaikki luetuiksi</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Load More Items..</source>
        <translation>Lataa lisää otsikoita..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show All Items</source>
        <translation>Näytä kaikki otsikot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show Unread Items</source>
        <translation>Näytä lukemattomat otsikot</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="371"/>
        <source>Open in Browser</source>
        <translation>Avaa web-selaimessa</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="375"/>
        <source>Copy URL</source>
        <translation>Kopioi URL</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>Peruuta jätä lukemattomaksi</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>Jätä lukemattomaksi</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Jaa</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="443"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation>Twitterin kirjautumistiedot poistettiin</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation>Instapaperin kirjautumistiedot poistettiin</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation>Facebookin kirjautumistiedot poistettiin</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation>Pocketin kirjautumistiedot poistettiin</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation>Tweettauksen lähetys onnistui</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation>Artikkelin lähetys Instapaperiin onnistui</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation>Facebook-viestin lähetys onnistui</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>Todentamisvirhe: yritä uudelleen kirjautumalla</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation>Tuntematon virhe: yritä uudelleen hetken kuluttua</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>關於應用程式</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation>版本 %1</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version 1.50</source>
        <translation>版本 1.50</translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation>版本 1.60 Beta</translation>
    </message>
    <message>
        <source>Version 1.60</source>
        <translation>版本 1.60</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Symbian^3上的Google Reader開源軟體.&lt;br&gt;&lt;br&gt;如需Bug回報、意見回饋以及功能需求 &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;請到此專案網頁&lt;/a&gt; 或是e-mail給作者 &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation>專案團隊</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation>作者 (twitter: @yogeshwarp)</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>介面意見回饋 &amp; 改善 / 美術設計&lt;br&gt;(twitter: @gx_saurav)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation>翻譯人員</translation>
    </message>
    <message>
        <source>Polish Language (twitter: @pagaw102)</source>
        <translation>波蘭語 (twitter: @pagaw102)</translation>
    </message>
    <message>
        <source>Simplified Chinese (twitter: @yeatse)</source>
        <translation>簡體中文 (twitter: @yeatse)</translation>
    </message>
    <message>
        <source>Traditional Chinese (twitter: @garykb8)</source>
        <translation>正體中文 (twitter: @garykb8)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation>德語</translation>
    </message>
    <message>
        <source>Bulgarian Language (twitter: @acnapyx)</source>
        <translation>保加利亞語 (twitter: @acnapyx)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation>芬蘭語</translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation>新增Feed到Google Reader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation>Feed網址/搜尋關鍵字</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation>標題 (非必要)</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation>關於gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation>使用淺色主題</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation>過濾全域未讀文章</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation>圖片自動縮圖</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation>完整HTML內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation>使用較大字體</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation>自動載入圖片</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation>啟用Swipe手勢</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation>清除登入資訊</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Dark Article Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="225"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>匯出OPML檔案 (需要登入Google帳號)</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="9"/>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="10"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation>新增或編輯標籤</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation>以逗號分隔標籤</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation>輸入標籤</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation>儲存</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="78"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="155"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="172"/>
        <source> of </source>
        <translation> / </translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation>標示為已讀取</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation>標示為未讀取</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation>移除星號</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation>新增星號</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation>傳送至</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation>搜尋結果</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation>訂閱</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>目前無法以新視窗開啟連結。如果您需要取回/註冊Google帳號，請使用手機的瀏覽器</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation>新增Feed到資料夾</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation>輸入新資料夾名稱</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation>執行</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>ago</source>
        <translation>以前</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>從現在</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>現在</translation>
    </message>
    <message>
        <source>min</source>
        <translation>分</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>分</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>小時</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>小時</translation>
    </message>
    <message>
        <source>day</source>
        <translation>天</translation>
    </message>
    <message>
        <source>days</source>
        <translation>天</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>週</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>週</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>月</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>月</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>年</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>年</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%1 %2 %3</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation>放開即可更新內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation>下拉可更新內容</translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation>登入至</translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation>登入帳號</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation>登入密碼</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation>保持登入狀態</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation>在此輸入文字</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation>傳送</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation>重新命名Feed</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Unsubscribe</source>
        <translation>取消訂閱</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <source>Mark All as Read</source>
        <translation>全部標示為已讀取</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Confirm Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="36"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="45"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation>移至資料夾</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation>訂閱內容</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>連線中發生錯誤 Error Code:</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="116"/>
        <source>Error in connection to Google</source>
        <translation>連線至Google發生錯誤</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="157"/>
        <source>Google Sign-In</source>
        <translation>登入Google</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="176"/>
        <location filename="../qml/gNewsReader/js/main.js" line="186"/>
        <source>Updating Subscriptions..</source>
        <translation>更新訂閱內容..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="333"/>
        <source>All News</source>
        <translation>所有新的內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="337"/>
        <source>All Unread</source>
        <translation>所有未讀內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="341"/>
        <source>All Starred</source>
        <translation>所有星號標記內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="350"/>
        <location filename="../qml/gNewsReader/js/main.js" line="358"/>
        <source>Loading Feed..</source>
        <translation>載入Feed..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="414"/>
        <source> of </source>
        <translation> / </translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="454"/>
        <source>You have no matching Feed Items</source>
        <translation>您沒有符合的Feed</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>You have no subscribed feeds</source>
        <translation>您沒有訂閱的Feeds</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>No Feeds with Unread Items</source>
        <translation>沒有未讀取的Feeds</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="689"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation type="unfinished">成功傳送連結至 (%1)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>成功傳送連結至</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service</source>
        <translation>成功傳送連結</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="692"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>無法登入，請檢查帳號或密碼是否有誤</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="696"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>傳送中發生未知的錯誤。請稍後再試</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="86"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="128"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="216"/>
        <source>Subscribe to New Feed</source>
        <translation>訂閱新的Feed</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show All</source>
        <translation>顯示全部內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show Unread</source>
        <translation>顯示未讀內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="232"/>
        <source>Load Starred Feeds</source>
        <translation>載入星號標記Feeds</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="224"/>
        <source>Load New Feeds</source>
        <translation>載入新的Feeds</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="228"/>
        <source>Load All Feeds</source>
        <translation>載入全部Feeds</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="236"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <source>Clear Authorization Data</source>
        <translation>清除登入資訊</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Confirm Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="290"/>
        <source>Mark All as Read</source>
        <translation>全部標示為已讀取</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Mark all as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Load More Items..</source>
        <translation>載入更多內容..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show All Items</source>
        <translation>顯示全部內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show Unread Items</source>
        <translation>顯示未讀內容</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="371"/>
        <source>Open in Browser</source>
        <translation>在瀏覽器中開啟</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="375"/>
        <source>Copy URL</source>
        <translation>複製連結</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>回復為已讀取</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>標示為未讀取</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>分享</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="443"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation>你已成功發送推訊</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation>你已成功分享至Facebook</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>認證失敗: 請重新登錄</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation>未知的錯誤: 請稍後再試</translation>
    </message>
</context>
</TS>

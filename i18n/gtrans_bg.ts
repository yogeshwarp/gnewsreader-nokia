<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="bg_BG">
<context>
    <name>AboutApplicationPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="13"/>
        <source>About Application</source>
        <translation>За приложението</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="51"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="52"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="56"/>
        <source>Open Source Google Reader Client for Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Клиент за Google Reader с отворен код за Symbian^3/MeeGo.&lt;br&gt;&lt;br&gt;За съобщения за бъгове, обратна връзка и молби за добавяне на функции&lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Посетете сайта на проекта&lt;/a&gt; или изпратете имейл на автора &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Йогешвар Падхиегуртжар&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="63"/>
        <source>Privacy Policy</source>
        <translation>Политика за поверителност</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="97"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="109"/>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;</source>
        <translation>Обратна връзка за интерфейса и графика</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="198"/>
        <source>Polish Language</source>
        <translation>Полски език</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="126"/>
        <source>Simplified Chinese</source>
        <translation>Опростен китайски език</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="138"/>
        <source>Traditional Chinese</source>
        <translation>Традиционен китайски език</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="162"/>
        <source>Bulgarian Language</source>
        <translation>Български език</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="186"/>
        <source>Croatian Language</source>
        <translation>Хърватски език</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="210"/>
        <source>Italian Language</source>
        <translation>Италиански език</translation>
    </message>
    <message>
        <source>Version 1.50</source>
        <translation>версия 1.50</translation>
    </message>
    <message>
        <source>Version 1.60 Beta</source>
        <translation>версия 1.60</translation>
    </message>
    <message>
        <source>Version 1.60</source>
        <translation>версия 1.60</translation>
    </message>
    <message>
        <source>Open Source Google Reader Client for Symbian^3.&lt;br&gt;&lt;br&gt;For Bug Reports, Feedback and Feature Requests &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Go to Project Website&lt;/a&gt; or e-mail author &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Yogeshwar Padhyegurjar&lt;br&gt;</source>
        <translation>Клиент за Google Reader с отворен код за Symbian^3.&lt;br&gt;&lt;br&gt;За съобщения за грешки, обратна връзка и молби за добавяне на функции &lt;a href=&quot;https://projects.developer.nokia.com/gNewsReader&quot;&gt;Посетете уебсайта на проекта&lt;/a&gt; или изпратете имейл на автора &lt;a href=&quot;mailto:yogeshwarp@ovi.com&quot;&gt;yogeshwarp@ovi.com&lt;/a&gt;&lt;br&gt;&lt;br&gt;Â© 2011 Йогешвар Падхиегуртжар&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="76"/>
        <source>Project Team</source>
        <translation>Екип на проекта</translation>
    </message>
    <message>
        <source>Author (twitter: @yogeshwarp)</source>
        <translation>Автор (twitter: @yogeshwarp)</translation>
    </message>
    <message>
        <source>UI Feedback &amp; Refinement / Artwork&lt;br&gt;(twitter: @gx_saurav)</source>
        <translation>Обратна връзка за интерфейса и графика&lt;br&gt;(twitter: @gx_saurav)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="116"/>
        <source>Translators</source>
        <translation>Преводачи</translation>
    </message>
    <message>
        <source>Polish Language (twitter: @pagaw102)</source>
        <translation>Полски език (twitter: @pagaw102)</translation>
    </message>
    <message>
        <source>Simplified Chinese (twitter: @yeatse)</source>
        <translation>Опростен китайски език (twitter: @yeatse)</translation>
    </message>
    <message>
        <source>Traditional Chinese (twitter: @garykb8)</source>
        <translation>Традиционен китайски език (twitter: @garykb8)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="150"/>
        <source>German Language</source>
        <translation>Немски език</translation>
    </message>
    <message>
        <source>Bulgarian Language (twitter: @acnapyx)</source>
        <translation>Български език (twitter: @acnapyx)</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AboutApplicationPage.qml" line="174"/>
        <source>Finnish Language</source>
        <translation>Фински език</translation>
    </message>
</context>
<context>
    <name>AddNewFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="6"/>
        <source>Add New Feed to Google Reader</source>
        <translation>Добави нов фийд към Google Reader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="21"/>
        <source>Feed URL/Search Term</source>
        <translation>URL на фийда/Ключова дума за търсене</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/AddNewFeedDialog.qml" line="27"/>
        <source>Title (Optional)</source>
        <translation>Заглавие (по желание)</translation>
    </message>
</context>
<context>
    <name>ApplicationSettings</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="12"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="36"/>
        <source>About gNewsReader</source>
        <translation>За gNewsReader</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="41"/>
        <source>Use Light Theme</source>
        <translation>Използвай светла тема</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="57"/>
        <source>Unread Filter Global</source>
        <translation>Глобален филтър на непрочетеното</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="73"/>
        <source>Auto Image Resize</source>
        <translation>Авто-оразмерявай снимки</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="89"/>
        <source>Full HTML Content</source>
        <translation>Пълно HTML съдържание</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="105"/>
        <source>Use Bigger Fonts</source>
        <translation>Използвай по-големи шрифтове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="121"/>
        <source>Auto Load Images</source>
        <translation>Авто-зареждане на снимките</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="137"/>
        <source>Enable Swipe Gesture</source>
        <translation>Разреши swipe жестове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="168"/>
        <source>Clear Authorization Data</source>
        <translation>Изчистване на данните за оторизацията</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="210"/>
        <source>Dark Article Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ApplicationSettings.qml" line="225"/>
        <source>Export OPML file (Requires Google Login)</source>
        <translation>Експорт на OPML файл (изисква влизане в Google)</translation>
    </message>
</context>
<context>
    <name>CommonQueryDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="9"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/CommonQueryDialog.qml" line="10"/>
        <source>Cancel</source>
        <translation>Отмяна</translation>
    </message>
</context>
<context>
    <name>EditTagsDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="6"/>
        <source>Add or Edit Tags</source>
        <translation>Добави/редактирай етикети</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="22"/>
        <source>Separate Tags by commas</source>
        <translation>Разделяй етикетите със запетаи</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="27"/>
        <source>Enter tags here</source>
        <translation>Въведете етикетите тук</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="46"/>
        <source>Save</source>
        <translation>Запис</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/EditTagsDialog.qml" line="58"/>
        <source>Cancel</source>
        <translation>Отмяна</translation>
    </message>
</context>
<context>
    <name>FeedItemPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="78"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="155"/>
        <location filename="../qml/gNewsReader/qml/FeedItemPage.qml" line="172"/>
        <source> of </source>
        <translation> от </translation>
    </message>
</context>
<context>
    <name>FeedListItemOptions</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="21"/>
        <source>Mark as Read</source>
        <translation>Маркирай като прочетени</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="32"/>
        <source>Keep Unread</source>
        <translation>Запази като непрочетено</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Remove Star</source>
        <translation>Премахни звезда</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="41"/>
        <source>Add Star</source>
        <translation>Отбележи със звезда</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedListItemOptions.qml" line="47"/>
        <source>Send To</source>
        <translation>Изпрати до</translation>
    </message>
</context>
<context>
    <name>FeedSearchResultPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="18"/>
        <source>Search Results</source>
        <translation>Резултати от търсене</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/FeedSearchResultPage.qml" line="98"/>
        <source>Subscribe</source>
        <translation>Абонамент</translation>
    </message>
</context>
<context>
    <name>GoogleOAuth2</name>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="178"/>
        <source>Opening Links that Lauch new Window is currently not supported. If you are trying to Recover/Create New Google Account please use Web Browser on your Device</source>
        <translation>Отварянето на линкове, които стартират нов прозорец, в момента не се поддържа. Ако се опитвате да възстановите/създадете нов акаунт в Google, моля използвайте уеб браузъра на вашето устройство</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/GoogleOAuth2.qml" line="190"/>
        <source>Close</source>
        <translation>Затваряне</translation>
    </message>
</context>
<context>
    <name>MoveFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="6"/>
        <source>Add Feed to Folder</source>
        <translation>Добави фийда към папка</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="36"/>
        <source>Enter New Folder Name</source>
        <translation>Въведете име на нова папка</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="41"/>
        <source>Go</source>
        <translation>Прилагане</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/MoveFeedDialog.qml" line="108"/>
        <source>Close</source>
        <translation>Затваряне</translation>
    </message>
</context>
<context>
    <name>OAuthConstants</name>
    <message>
        <source>gNewsReader application does not use any personal data for analytics, in fact there is no analytics solution being used.

All the user authorization for various sharing services (twitter/facebook/instapaper/pocket) is collected by user concent only and optional.
All authorization information is only stored on the phone in application specific storage and not in shared area. All network call involving transfer of authorization information is made over secure HTTP channel. Additionally authorization information stored locally can be cleared anytime by the user.

To clear google id authorization use main application menu, any sharing services stored authorization can be cleared using settings page</source>
        <translation>Приложението gNewsReader не използва никакви лични данни за анализи, всъщност не се използва никакво решение за анализи.

Данни за оторизацията на потребителя в различни услуги за споделяне (Twitter/Facebook/Instapaper/Pocket) се събира единствено със съгласието на потребителя и е опционална.
Всичката информация за оторизацията се съхранява само в телефона в специфично за приложението хранилище и не се споделя с други приложения. Всички мрежови заявки, свързани с трансфер на информация за оторизацията се извършват през защитен HTTP канал. Освен това информацията за оторизациите, съхранявана локално, може да бъде изтрита по всяко време от потребителя.

За да премахнете оторизацията с Google ID, използвайте основното меню на приложението, а съхранената оторизация в услуги за споделяне може да бъде премахната от страницата с настройки</translation>
    </message>
    <message>
        <source>ago</source>
        <translation>преди</translation>
    </message>
    <message>
        <source>From Now</source>
        <translation>Отсега</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>току-що</translation>
    </message>
    <message>
        <source>min</source>
        <translation>мин</translation>
    </message>
    <message>
        <source>mins</source>
        <translation>мин</translation>
    </message>
    <message>
        <source>hr</source>
        <translation>ч</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>ч</translation>
    </message>
    <message>
        <source>day</source>
        <translation>ден</translation>
    </message>
    <message>
        <source>days</source>
        <translation>дни</translation>
    </message>
    <message>
        <source>wk</source>
        <translation>седм</translation>
    </message>
    <message>
        <source>wks</source>
        <translation>седм</translation>
    </message>
    <message>
        <source>mth</source>
        <translation>мес</translation>
    </message>
    <message>
        <source>mths</source>
        <translation>мес</translation>
    </message>
    <message>
        <source>yr</source>
        <translation>г</translation>
    </message>
    <message>
        <source>yrs</source>
        <translation>г</translation>
    </message>
    <message>
        <source>%1 %2 %3</source>
        <comment>e.g. %1 is number value such as 2, %2 is mins, %3 is ago</comment>
        <translation>%3 %1 %2</translation>
    </message>
</context>
<context>
    <name>PullToActivate</name>
    <message>
        <source>Click to Refresh</source>
        <translation>Щракни за опресняване</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull up to Load More</source>
        <translation>Издърпай нагоре за зареждане на още</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Refresh</source>
        <translation>Отпусни за опресняване</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="17"/>
        <source>Release to Load More</source>
        <translation>Отпусни за зареждане на още</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/PullToActivate.qml" line="16"/>
        <source>Pull down to Refresh</source>
        <translation>Издърпай надолу за опресняване</translation>
    </message>
</context>
<context>
    <name>ReadLaterLoginDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Login to </source>
        <translation>Влизане в </translation>
    </message>
    <message>
        <source>Read It Later</source>
        <translation>Read It Later</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Instapaper</source>
        <translation>Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="10"/>
        <source>Pocket</source>
        <translation>Pocket</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="11"/>
        <source>Share Article with URL</source>
        <translation>Сподели публикацията с URL</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="35"/>
        <source>Login Id</source>
        <translation>Потребителско име</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="42"/>
        <source>Password</source>
        <translation>Парола</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="51"/>
        <source>Save Login Information</source>
        <translation>Запис на името и паролата</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="57"/>
        <source>Privacy Policy</source>
        <translation>Политика за поверителност</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="81"/>
        <source>Enter Text Here</source>
        <translation>Въведете текст тук</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Send</source>
        <translation>Изпращане</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="140"/>
        <source>Login</source>
        <translation>Влизане</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml" line="160"/>
        <source>Cancel</source>
        <translation>Отмяна</translation>
    </message>
</context>
<context>
    <name>RenameFeedDialog</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="6"/>
        <source>Rename Feed</source>
        <translation>Преименувай фийд</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="40"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/RenameFeedDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>Отмяна</translation>
    </message>
</context>
<context>
    <name>SubItemOptionsMenu</name>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="50"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Unsubscribe</source>
        <translation>Прекратяване на абонамент</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="32"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <source>Mark All as Read</source>
        <translation>Маркирай всичко като прочетено</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="33"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="52"/>
        <source>Confirm Action</source>
        <translation>Потвърди действието</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="36"/>
        <source>Rename</source>
        <translation>Преименувай</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="45"/>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="47"/>
        <source>Delete</source>
        <translation>Изтрий</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/components/SubItemOptionsMenu.qml" line="55"/>
        <source>Move to Folder</source>
        <translation>Премести в папка</translation>
    </message>
</context>
<context>
    <name>SubscriptionsPage</name>
    <message>
        <location filename="../qml/gNewsReader/qml/SubscriptionsPage.qml" line="42"/>
        <source>Subscriptions</source>
        <translation>Абонаменти</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="44"/>
        <source>Error Ocurred in Connection. Error Code:</source>
        <translation>Възникна грешка при свързване. Код на грешката:</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="116"/>
        <source>Error in connection to Google</source>
        <translation>Грешка при свързване с Google</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="157"/>
        <source>Google Sign-In</source>
        <translation>Влизане в Google</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="176"/>
        <location filename="../qml/gNewsReader/js/main.js" line="186"/>
        <source>Updating Subscriptions..</source>
        <translation>Обновяване на абонаментите..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="333"/>
        <source>All News</source>
        <translation>Всички нови</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="337"/>
        <source>All Unread</source>
        <translation>Всички непрочетени</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="341"/>
        <source>All Starred</source>
        <translation>Всички отб. със звезда</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="350"/>
        <location filename="../qml/gNewsReader/js/main.js" line="358"/>
        <source>Loading Feed..</source>
        <translation>Зареждане на фийд..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="414"/>
        <source> of </source>
        <translation> от </translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="454"/>
        <source>You have no matching Feed Items</source>
        <translation>Няма такива публикации във фийда</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>You have no subscribed feeds</source>
        <translation>Няма абонамент за фийдове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="623"/>
        <source>No Feeds with Unread Items</source>
        <translation>Няма непрочетени фийдове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="689"/>
        <source>Successfully sent the link to Service (%1)</source>
        <comment>%1 will be replaced by service name such as Facebook by program</comment>
        <translation>Връзката бе успешно изпратена до услугата (%1)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service (params)</source>
        <translation>Връзката бе успешно изпратена до услугата (параметри)</translation>
    </message>
    <message>
        <source>Successfully sent the link to Service</source>
        <translation>Линкът бе успешно изпратен до услугата</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="692"/>
        <source>Unable to Login to Service, Username or Password could be Wrong</source>
        <translation>Невъзможно влизане в услугата, името или паролата може би са погрешни</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/main.js" line="696"/>
        <source>Unknown error while sending. Please try again after some time</source>
        <translation>Неизвестна грешка при изпращане. Моля опитайте отново след известно време</translation>
    </message>
</context>
<context>
    <name>newsreader</name>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="86"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="128"/>
        <source>gNewsReader</source>
        <translation>gNewsReader</translation>
    </message>
    <message>
        <source>Go Online</source>
        <translation>Мини онлайн</translation>
    </message>
    <message>
        <source>Go Offline</source>
        <translation>Мини офлайн</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="216"/>
        <source>Subscribe to New Feed</source>
        <translation>Абониране за нов фийд</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show All</source>
        <translation>Покажи всички</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="220"/>
        <source>Show Unread</source>
        <translation>Покажи непрочетените</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="232"/>
        <source>Load Starred Feeds</source>
        <translation>Зареди фийдовете със звезда</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="224"/>
        <source>Load New Feeds</source>
        <translation>Зареди нови фийдове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="228"/>
        <source>Load All Feeds</source>
        <translation>Зареди всички фийдове</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="236"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <source>Clear Authorization Data</source>
        <translation>Изчистване на данните за оторизацията</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="237"/>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Confirm Action</source>
        <translation>Потвърди действието</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="290"/>
        <source>Mark All as Read</source>
        <translation>Маркирай всички като прочетени</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="291"/>
        <source>Mark all as Read</source>
        <translation>Маркирай всичко като прочетено</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="295"/>
        <source>Load More Items..</source>
        <translation>Зареди още..</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show All Items</source>
        <translation>Покажи всички</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="299"/>
        <source>Show Unread Items</source>
        <translation>Покажи непрочетените</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="371"/>
        <source>Open in Browser</source>
        <translation>Отвори в браузър</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="375"/>
        <source>Copy URL</source>
        <translation>Копирай URL</translation>
    </message>
    <message>
        <source>Undo Keep Unread</source>
        <translation>Не запазвай като непрочетено</translation>
    </message>
    <message>
        <source>Keep Unread</source>
        <translation>Запази като непрочетено</translation>
    </message>
    <message>
        <source>Share</source>
        <translation>Споделяне</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/qml/newsreader.qml" line="443"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
</context>
<context>
    <name>shareArticleService</name>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="38"/>
        <source>Twitter Authorization Cleared Successfully</source>
        <translation>Оторизирането в Twitter бе премахнато успешно</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="44"/>
        <source>Instapaper Authorization Cleared Successfully</source>
        <translation>Оторизирането в Instapaper бе премахнато успешно</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="49"/>
        <source>Facebook Authorization Cleared Successfully</source>
        <translation>Оторизирането във Facebook бе премахнато успешно</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="55"/>
        <source>Pocket Authorization Cleared Successfully</source>
        <translation>Оторизирането в Pocket бе премахнато успешно</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="75"/>
        <source>Your tweet was posted successfully</source>
        <translation>Туитът ви бе успешно публикуван</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="95"/>
        <source>Your article was posted successfully to Instapaper</source>
        <translation>Постът ви бе успешно публикуван в Instapaper</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="141"/>
        <source>Your post was shared on Facebook successfully</source>
        <translation>Постът ви бе успешно споделен във Facebook</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="153"/>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="156"/>
        <source>Authentication Error: Please reauthorize with login</source>
        <translation>Грешка при идентификация: Оторизирайте се отново</translation>
    </message>
    <message>
        <location filename="../qml/gNewsReader/js/shareArticleService.js" line="157"/>
        <source>Unknows Error: Please retry after some time</source>
        <translation>Неизвестна грешка: Опитайте след известно време</translation>
    </message>
</context>
</TS>

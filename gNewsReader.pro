QT += network declarative webkit

# Add more folders to ship with the application, here
folder_01.source = qml/gNewsReader
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

#symbian:TARGET.UID3 = 0xE0A3B071 #Nokia Developer Project
#symbian:TARGET.UID3 = 0x20057B10 #SymbianSigned
symbian:TARGET.UID3 = 0x20049f95

VERSION = 1.83.312
MEEGOVERSION = 1.8.3

CONFIG += qt-components

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
#symbian:TARGET.CAPABILITY += NetworkServices
symbian {
    TARGET.CAPABILITY += NetworkServices
    TARGET.CAPABILITY += SwEvent
    TARGET.CAPABILITY += ReadUserData
    TARGET.CAPABILITY += ReadDeviceData
    TARGET.CAPABILITY += WriteDeviceData

    vendorinfo = "%{\"Yogeshwar Padhyegurjar\"}" ":\"Yogeshwar Padhyegurjar\""
    LIBS += -lapgrfx\
            -leikcore \
            -lcone \
            -lapmime

    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x400000 0x6000000 #64MB

    my_deployment.pkg_prerules += vendorinfo

    my_deployment.pkg_prerules += \
        "; Dependency to Symbian Qt Quick components" \
        "(0x200346DE), 1, 1, 0, {\"Qt Quick components\"}"

    DEPLOYMENT += my_deployment

    OTHER_FILES += \
        qml/gNewsReader/qml/*.qml \
        qml/gNewsReader/js/*.js \
        qml/gNewsReader/css/*.css \
        qml/gNewsReader/pics/*.*

    qmlfiles.sources = qml
    DEPLOYMENT += qmlfiles
    DEFINES += APP_VERSION=\"$$VERSION\"
}

win32 {
    # Create a define APP_VERSION and set its value to our version.
    # The APP_VERSION will be visible to the source code side.
    DEFINES += APP_VERSION=\\\"$$VERSION\\\"
}

# Harmattan specific
contains(MEEGO_EDITION, harmattan) {
    DEFINES += Q_WS_HARMATTAN
#    target.path = /opt/usr/bin
#    INSTALLS += target

    OTHER_FILES += \
        qtc_packaging/debian_harmattan/rules \
        qtc_packaging/debian_harmattan/README \
        qtc_packaging/debian_harmattan/manifest.aegis \
        qtc_packaging/debian_harmattan/copyright \
        qtc_packaging/debian_harmattan/control \
        qtc_packaging/debian_harmattan/compat \
        qtc_packaging/debian_harmattan/changelog \
        qml/gNewsReader/meego/*.qml \
        qml/gNewsReader/js/*.js \
        qml/gNewsReader/css/*.css \
        qml/gNewsReader/pics/*.*

    qmlfiles.path = qml/gNewsReader/
    qmlfiles.files += meego/*
    INSTALLS += qmlfiles
    DEFINES += APP_VERSION=\\\"$$MEEGOVERSION\\\"
}

#To use hardware FPU for floating point calculations
symbian {
        MMP_RULES += "OPTION gcce -march=armv6"
        MMP_RULES += "OPTION gcce -mfpu=vfp"
        MMP_RULES += "OPTION gcce -mfloat-abi=softfp"
        MMP_RULES += "OPTION gcce -marm"
}

symbian {

}


# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    customnetworkaccessmanager.cpp \
    useragentprovider.cpp \
    networkaccessmanagerfactory.cpp \
    subitem.cpp \
    subitemfilter.cpp \
    mywebview.cpp \
    fileio.cpp \
    setting.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    customnetworkaccessmanager.h \
    useragentprovider.h \
    networkaccessmanagerfactory.h \
    subitem.h \
    subitemfilter.h \
    mywebview.h \
    fileio.h \
    setting.h

evil_hack_to_fool_lupdate {
    SOURCES += \
    qml/gNewsReader/qml/newsreader.qml \
    qml/gNewsReader/qml/SubscriptionsPage.qml \
    qml/gNewsReader/qml/FeedListPage.qml \
    qml/gNewsReader/qml/FeedItemPage.qml \
    qml/gNewsReader/qml/GoogleOAuth2.qml \
    qml/gNewsReader/qml/components/AboutApplicationPage.qml \
    qml/gNewsReader/qml/components/AddNewFeedDialog.qml \
    qml/gNewsReader/qml/components/ApplicationSettings.qml \
    qml/gNewsReader/qml/components/EditTagsDialog.qml \
    qml/gNewsReader/qml/components/FeedListItemOptions.qml \
    qml/gNewsReader/qml/components/FeedSearchResultPage.qml \
    qml/gNewsReader/qml/components/MoveFeedDialog.qml \
    qml/gNewsReader/qml/components/ReadLaterLoginDialog.qml \
    qml/gNewsReader/qml/components/RenameFeedDialog.qml \
    qml/gNewsReader/qml/components/SubItemOptionsMenu.qml \
    qml/gNewsReader/qml/components/CommonQueryDialog.qml \
    qml/gNewsReader/qml/PullToActivate.qml \
    qml/gNewsReader/js/main.js \
    qml/gNewsReader/js/OAuthConstants.js \
    qml/gNewsReader/js/shareArticleService.js
}

TRANSLATIONS += i18n/gtrans_de.ts \
                i18n/gtrans_fr.ts \
                i18n/gtrans_bn.ts \
                i18n/gtrans_it.ts \
                i18n/gtrans_zh.ts \
                i18n/gtrans_zh_TW.ts \
                i18n/gtrans_fi.ts \
                i18n/gtrans_pl.ts \
                i18n/gtrans_hr.ts \
                i18n/gtrans_bg.ts

RESOURCES += \
    i18n/newsreader.qrc

contains(MEEGO_EDITION,harmattan) {
    icon.files = gNewsReader-meego.svg
    icon.path = /usr/share/icons/hicolor/scalable/apps
    INSTALLS += icon
}

contains(MEEGO_EDITION,harmattan) {
    desktopfile.files = gnewsreader.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
}

import QtQuick 1.1
import com.nokia.meego 1.0

ContextMenu {
    id: feedItemOptionsMenu
    property int feedIndex: 0
    property string feedId
    property string feedUrl
    property bool readstatus
    property bool keptUnread
    property bool starred
    property string categories
    property string title
    property string articleUrl

    onStatusChanged: if(status == DialogStatus.Closed) feedListPage.takeFocus()

    MenuLayout {
        MenuItem {
            id: menuMarkAsRead
            text: qsTr("Mark as Read")
            //platformLeftMargin: 2 * meegoStyle.paddingMedium
            visible: !feedItemOptionsMenu.readstatus
            onClicked: {
                feedListPage.toggleTagStatus(feedItemOptionsMenu.feedIndex, feedItemOptionsMenu.feedId, feedItemOptionsMenu.feedUrl, "readstatus", feedItemOptionsMenu.readstatus, "user/-/state/com.google/read")
                if(feedItemOptionsMenu.keptUnread) feedListPage.toggleTagStatus(feedItemOptionsMenu.feedIndex, feedItemOptionsMenu.feedId, feedItemOptionsMenu.feedUrl, "keptUnread", feedItemOptionsMenu.keptUnread, "user/-/state/com.google/kept-unread")
                feedListPage.updateFeedCount(feedItemOptionsMenu.feedUrl, feedItemOptionsMenu.categories, 1)
            }
        }
        MenuItem {
            //platformLeftMargin: 2 * meegoStyle.paddingMedium
            text: qsTr("Keep Unread")
            visible: feedItemOptionsMenu.readstatus && !feedItemOptionsMenu.keptUnread
            onClicked: {
                feedListPage.toggleTagStatus(feedItemOptionsMenu.feedIndex, feedItemOptionsMenu.feedId, feedItemOptionsMenu.feedUrl, "readstatus", feedItemOptionsMenu.readstatus, "user/-/state/com.google/read")
                feedListPage.toggleTagStatus(feedItemOptionsMenu.feedIndex, feedItemOptionsMenu.feedId, feedItemOptionsMenu.feedUrl, "keptUnread", feedItemOptionsMenu.keptUnread, "user/-/state/com.google/kept-unread")
                feedListPage.updateFeedCount(feedItemOptionsMenu.feedUrl, feedItemOptionsMenu.categories, -1)
            }
        }
        MenuItem {
            text: feedItemOptionsMenu.starred ? qsTr("Remove Star") : qsTr("Add Star")
            //platformLeftMargin: 2 * meegoStyle.paddingMedium
            onClicked: feedListPage.toggleTagStatus(feedItemOptionsMenu.feedIndex, feedItemOptionsMenu.feedId, feedItemOptionsMenu.feedUrl, "starred", feedItemOptionsMenu.starred, "user/-/state/com.google/starred")
        }
        MenuItem {
            //platformLeftMargin: 2 * meegoStyle.paddingMedium
            text: qsTr("Send To")
            ButtonRow {
                    width: 0.65*parent.width
                    exclusive: false
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: meegoStyle.paddingLarge
                    ToolButton {
                        iconSource: "../../pics/twitter.svg"
                        platformStyle: ButtonStyle{ inverted: true }
                        onClicked: {
                            feedListPage.shareToReadLater("TWITTER", feedItemOptionsMenu.title, feedItemOptionsMenu.articleUrl)
                            feedItemOptionsMenu.close()
                        }
                    }
                    ToolButton {
                        iconSource: "../../pics/facebook.svg"
                        platformStyle: ButtonStyle{ inverted: true }
                        onClicked: {
                            feedListPage.shareToReadLater("FACEBOOK", feedItemOptionsMenu.title, feedItemOptionsMenu.articleUrl)
                            feedItemOptionsMenu.close()
                        }
                    }
                    ToolButton {
                            iconSource: "../../pics/read_it_later.svg"
                            platformStyle: ButtonStyle{ inverted: true }
                            onClicked: { feedListPage.shareToReadLater("READ_IT_LATER", feedItemOptionsMenu.title, feedItemOptionsMenu.articleUrl); feedItemOptionsMenu.close() }
                    }
                    ToolButton {
                            iconSource: "../../pics/instapaper.svg"
                            platformStyle: ButtonStyle{ inverted: true }
                            onClicked: { feedListPage.shareToReadLater("INSTAPAPER", feedItemOptionsMenu.title, feedItemOptionsMenu.articleUrl); feedItemOptionsMenu.close() }
                    }

            }
        }
    }
}

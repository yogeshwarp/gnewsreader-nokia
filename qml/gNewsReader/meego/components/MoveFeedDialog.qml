import QtQuick 1.1
import com.nokia.meego 1.0
import ".." 1.1

CommonDialog {
    id: moveFeedDialog
    titleText: qsTr("Add Feed to Folder")

    property ListModel model
    model: ListModel {
        id: selectTagListModel
    }
    property string currFoldersStr: ""
    property string feedId: ""
    property string feedTitle: ""

    //width: parent.width// - 2*meegoStyle.paddingMedium
    height: window.inPortrait ? window.height*0.8 : window.height

    content: Column {
        width: parent.width
        height: window.inPortrait ? 550 : 300
        spacing: meegoStyle.paddingMedium
        anchors {
            left: parent.left
            top: parent.top
            margins: meegoStyle.paddingMedium
        }

        Row {
            id: goButtonRow
            width: parent.width - 2*meegoStyle.paddingMedium
            spacing: meegoStyle.paddingMedium
            //z: 2
            TextField {
                id: newFolderText
                placeholderText: qsTr("Enter New Folder Name")
                width: 0.8*parent.width
            }
            Button {
                id: goButton
                text: qsTr("Go")
                //width: 2*meegoStyle.paddingLarge
                platformStyle: ButtonStyle {
                    inverted: true
                    buttonWidth: 144
                    position: "[horizontal-right]"
                }
                onClicked: {
                    subscrListPage.itemOptions(moveFeedDialog.feedId, newFolderText.text, "move", moveFeedDialog.feedTitle)
                    moveFeedDialog.close()
                }
            }
        }

        ListView {
            id: selectFolderListView
            clip: true
            width: parent.width
            height: parent.height - goButtonRow.height

            model: moveFeedDialog.model

            delegate: ListItem {
                id: tagListItem
                width: selectFolderListView.width
                height: feedNewParentCb.height + 2*meegoStyle.paddingMedium
                Row {
                    width: parent.width
                    //anchors.fill: tagListItem.paddingItem
                    anchors {
                        top: tagListItem.top
                        left: tagListItem.left
                        margins: meegoStyle.paddingMedium
                    }
                    spacing: meegoStyle.paddingMedium

                    CheckBox {
                        id: feedNewParentCb
                        checked: selected
                        platformStyle: CheckBoxStyle { inverted: true }
                        text: name
                        onClicked: {
                            //console.log("Clicked on "+name+" current selection status:"+checked)
                            addRemoveFolder(moveFeedDialog.feedId, moveFeedDialog.feedTitle)
                        }
                    }

//                    ListItemText {
//                        text: name
//                        anchors.verticalCenter: feedNewParentCb.verticalCenter
//                        verticalAlignment: Text.AlignVCenter
//                    }
                }
                onClicked: { feedNewParentCb.checked = !feedNewParentCb.checked; addRemoveFolder(moveFeedDialog.feedId, moveFeedDialog.feedTitle) }

                function addRemoveFolder(feedId, feedTitle) {
                    if(selected != feedNewParentCb.checked) {
                        subscrListPage.itemOptions(feedId, name, feedNewParentCb.checked ? "move" : "remove", feedTitle)
                        selectFolderListView.model.setProperty(index, "selected", feedNewParentCb.checked)
                    }
                }
            }
        }
    }

    buttons: ButtonRow  {
        width: parent.width - 2*meegoStyle.paddingMedium
        height: moveFeedDialogButtonClose.height + meegoStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: meegoStyle.paddingMedium
        }
        Button {
            id: moveFeedDialogButtonClose
            text: qsTr("Close")
            platformStyle: ButtonStyle { inverted: true; position: "[horizontal-center]" }
            onClicked: moveFeedDialog.close()
            width: parent.width
        }
    }
    //onClickedOutside: moveFeedDialog.close()
}

import QtQuick 1.1
import com.nokia.symbian 1.1

CommonDialog {
    id: subFeedDialog
    titleText: qsTr("Add New Feed to Google Reader")

    property string feedUrl: ""
    property string feedFolder: ""

    content: Column {
        width: parent.width - 2*platformStyle.paddingMedium
        height: feedUrlText.height + feedTitleText.height + 3*platformStyle.paddingMedium
        spacing: platformStyle.paddingMedium
        anchors {
            top: parent.top
            left: parent.left
            margins: platformStyle.paddingMedium
        }
        TextField {
            placeholderText: qsTr("Feed URL/Search Term")
            id: feedUrlText
            text: feedUrl
            width: parent.width
        }
        TextField {
            placeholderText: qsTr("Title (Optional)")
            id: feedTitleText
            text: feedFolder
            width: parent.width
        }
    }

    buttons: Row  {
        spacing: platformStyle.paddingMedium
        width: parent.width - 2*platformStyle.paddingMedium
        height: addNewFeedDialogButtonOk.height + platformStyle.paddingMedium
        //exclusive: false
        anchors {
            top: parent.top
            left: parent.left
            margins: platformStyle.paddingMedium
        }
        Button {
            width: (parent.width - 2*platformStyle.paddingMedium)*0.2
            iconSource: "../../pics/tb_paste.svg"
            onClicked: feedUrlText.paste()
        }
        Button {
            id: addNewFeedDialogButtonOk
            width: (parent.width - 2*platformStyle.paddingMedium)*0.4
            iconSource: "../../pics/tb_ok.svg"
            onClicked: {
                if(feedUrlText.text != undefined && feedUrlText.text != "") {
                    if(/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(feedUrlText.text)) {
                    subscrListPage.itemOptions("feed/"+feedUrlText.text, -1, "subscribe", feedTitleText.text)
                    subFeedDialog.close()
                    } else {
                        subscrListPage.searchForFeed(feedUrlText.text); subFeedDialog.close()
                    }
                }
            }
        }
//                Button {
//                    iconSource: "../pics/tb_search.svg"
//                    onClicked: { subscrListPage.searchForFeed(feedUrlText.text); subFeedDialog.close() }
//                }

        Button {
            width: (parent.width - 2*platformStyle.paddingMedium)*0.4
            iconSource: "../../pics/tb_close_stop.svg"
            onClicked: subFeedDialog.close()
        }
    }
    onClickedOutside: { subFeedDialog.close() }
}

var WebRequest; if (WebRequest == null) WebRequest = {};

WebRequest.setProperties = function setProperties(into, from) {
    if (into != null && from != null) {
        for (var key in from) {
            into[key] = from[key];
        }
    }
    return into;
}

WebRequest.authMethod = "none" //Use "none", "oauth2" and "oauth"
WebRequest.authToken = "" // Set from caller as appropriate
WebRequest.authService = "" // Use "Google", "Twitter", "Facebook", "Pocket", "Instapaper" and "Readability"
WebRequest.method = "GET" //"POST" also supported
WebRequest.url = null //url to use

WebRequest.onCallback = function onCallback(data) {} //empty function
WebRequest.onError = function onError(errorCode) {} //empty function

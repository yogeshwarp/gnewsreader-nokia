function doXmlHttpRequest(method, url, params, callback, caller, callerarg) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            callback(req.responseText, caller, callerarg);
        }
    }
    console.log(url)
    req.open(method, url);
//    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//    req.setRequestHeader("Content-Length", String(params.length));
    req.send(/*params*/);
}

function getMobilizedView(url) {
    var urlGoogle = "http://www.google.com/gwt/x?noimg=1&u="
    var urlInstapaper = "http://www.instapaper.com/m?u="
    var urlReadability = "http://www.readability.com/m?url="
    doXmlHttpRequest("GET", urlReadability + encodeURIComponent(url), null, setMobilizedView, null, null)
}

function setMobilizedView(data) {
    //console.log("Page obtained from mobilizer:"+data)
    WorkerScript.sendMessage({ 'reply': data })
}

WorkerScript.onMessage = function(message) {
    // ... long-running operations and calculations are done here
    getMobilizedView(message)
    //WorkerScript.sendMessage({ 'reply': 'Mouse is at ' + message.x + ',' + message.y })
}
